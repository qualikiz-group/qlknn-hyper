This version of the QuaLiKiz neural network is free for general use.
Development details and citation are found in (van de Plassche et al., [2020](https://doi.org/10.1063/1.5134126)) 

Please cite this work when using the model in your workflow. 

For more information contact Karel van de Plassche at k.l.vandeplassche@differ.nl or karelvandeplassche@gmail.com
